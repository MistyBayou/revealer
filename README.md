# Simple Revealer (WIP)

## Fill in `config.json`

Mostly self-explanatory.

Make sure there are only images in `imageDirectory` (no zip file etc).

Create `offChainDataDirectory` and `metabossDataDirectory` as empty dirs.

Database file should not exist.

Set `totalReveals` to the number of reveals you want TOTAL (not for this batch).

## Fund the Arweave Bundler Node

We're using Arweave Bundlr because it guarantees success (eventually).

```
npm install -g @bundlr-network/client

bundlr fund 1000000000000 -h http://node1.bundlr.network -w wallet.json -c arweave
```

This command will fund the node with 1 AR. Modify as to your need.

Wait about 30 minutes before trying to run the script.

Read [the documentation](https://docs.bundlr.network/docs/client/cli#fund-a-bundlr-node) for more info.

## Install dependencies

Install [metaboss](https://github.com/textury/arkb) and [arkb](https://github.com/textury/arkb).

Install yarn dependencies: `yarn install`

## Run

```
node --loader ts-node/esm src/go.ts
```
