interface LocalConfig {
  mintListFile: string,
  imageDirectory: string,
  offChainDataDirectory: string,
  metabossDataDirectory: string,
  databaseFile: string,
  pathToMetaboss: string,
  totalReveals: number
}

interface ArweaveConfig {
  privateKeyFile: string
}

interface SolanaConfig {
  privateKeyFile: string,
  rpcServer: string | null
}

interface Creator {
  address: string,
  verified: number,
  share: number
}

interface NftCollectionConfig {
  name: string,
  family: string
}

interface NftConfig {
  symbol: string,
  description: string,
  collection: NftCollectionConfig,
  sellerFeeBasisPoints: 1250,
  creators: Creator[]
}

export interface Config {
  local: LocalConfig,
  arweave: ArweaveConfig,
  solana: SolanaConfig,
  nft: NftConfig
}
