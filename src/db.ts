import { Config } from './config'
import { State } from './state'

interface Trait {
  traitName: string,
  traitValue: string
}

export interface Reveal {
  imageName: string,
  mintAddress: string,
  state: State,
  name: string | null,
  traits: Trait[],
  imageArweaveTx: string | null,
  offChainDataArweaveTx: string | null,
  offChainDataJsonFile: string | null,
  dataJson: Object | null,
  verificationAttempts: number,
  final?: () => void
}

export interface Db {
  config: Config,
  reveals: Reveal[]
  unallocatedMints: string[],
  unallocatedImages: string[],
  leftToAllocate: number
}
