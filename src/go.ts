import { Config } from './config'
import { Db, Reveal } from './db'
import { State } from './state'

import { sprintf } from 'sprintf-js'
import * as fs from 'fs'

import Bundlr from '@bundlr-network/client'
import axios from 'axios'

const fgCyan = "\x1b[36m"
const fgMagenta = "\x1b[35m"
const fgYellow = "\x1b[33m"
const commandColors = '\x1b[40m\x1b[32m'

const initDb = async (config: Config): Promise<Db> => {
  console.log('Preparing new database.')

  return {
    config: config,
    reveals: [],
    unallocatedMints: JSON.parse(fs.readFileSync(config.local.mintListFile).toString()),
    unallocatedImages: fs.readdirSync(config.local.imageDirectory),
    leftToAllocate: config.local.totalReveals
  }
}

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))

const logPrefix = (reveal: Reveal) => `${(new Date()).toTimeString()} [${reveal.mintAddress}] `

const loadDb = async (config: Config): Promise<Db> => {
  console.log('Loading existing database from file.')

  const loadedDb: Db = JSON.parse(fs.readFileSync(config.local.databaseFile).toString())

  const mints: string[] = JSON.parse(fs.readFileSync(config.local.mintListFile).toString())
  const images: string[] = fs.readdirSync(config.local.imageDirectory)

  return {
    config: config,
    reveals: loadedDb.reveals,
    unallocatedMints: mints.filter(m => !loadedDb.reveals.some((r: Reveal) => r.mintAddress === m)),
    unallocatedImages: images.filter(i => !loadedDb.reveals.some((r: Reveal) => r.imageName === i)),
    leftToAllocate: config.local.totalReveals - loadedDb.reveals.length
  }
}

const saveDb = async (db: Db): Promise<Db> => {
  const dbJson = JSON.stringify(db)
  fs.writeFileSync(db.config.local.databaseFile, dbJson)

  return db
}

const allocate = async (db: Db): Promise<Db> => {
  if (db.leftToAllocate === 0) {
    return await saveDb(db)
  }

  if (db.leftToAllocate > db.unallocatedImages.length || db.leftToAllocate > db.unallocatedMints.length) {
    console.log(fgMagenta, 'Cannot allocate more mints than images/mints available.')

    throw new Error('')
  }

  const randomImage = db.unallocatedImages[Math.floor(Math.random() * db.unallocatedImages.length)]
  const randomMint = db.unallocatedMints[Math.floor(Math.random() * db.unallocatedMints.length)]

  console.log(fgCyan, `Allocating image '${randomImage}' to mint '${randomMint}'.`, '\n')

  return await allocate({
    config: db.config,
    reveals: [
      ...db.reveals,
      {
        imageName: randomImage,
        mintAddress: randomMint,
        name: null, 
        traits: [],
        state: State.Allocated,
        imageArweaveTx: null,
        offChainDataArweaveTx: null,
        offChainDataJsonFile: null,
        dataJson: null,
        verificationAttempts: 0
      }
    ],
    unallocatedImages: db.unallocatedImages.filter(i => i !== randomImage),
    unallocatedMints: db.unallocatedMints.filter(m => m !== randomMint),
    leftToAllocate: db.leftToAllocate - 1
  })
}

const titleToSentence = (s: string) => s.replace(/([A-Z])/g, ' $1').trim()

const extractNameAndTraits = async (reveal: Reveal, _config: Config): Promise<Reveal> => {
  const filenameParts = reveal.imageName.split('-')
  const body = filenameParts[5].split('.')

  return {
    ...reveal,
    state: State.NameAndTraitsExtracted,
    name: titleToSentence(filenameParts[0]),
    traits: [
      { traitName: 'tribe', traitValue: titleToSentence(filenameParts[1]) },
      { traitName: 'background', traitValue: titleToSentence(filenameParts[2]) },
      { traitName: 'head', traitValue: titleToSentence(filenameParts[3]) },
      { traitName: 'face', traitValue: titleToSentence(filenameParts[4]) },
      { traitName: 'body', traitValue: titleToSentence(body[0]) }
    ]
  }
}

const uploadFile = async (
  bundlr: Bundlr,
  reveal: Reveal,
  successState: State,
  filename: string,
  contentType: string
): Promise<{itemId: string, state: State}> => {
  console.log(fgCyan, logPrefix(reveal), `Uploading ${filename} to Arweave.`)
  const data = fs.readFileSync(filename)

  const price = await bundlr.getPrice(data.length)
  const balance = await bundlr.getLoadedBalance()

  if (price.isGreaterThan(balance)) {
    console.log(fgMagenta, logPrefix(reveal), 'You need to top up the Arweave bundler.')
    console.log(fgMagenta, logPrefix(reveal), 'See https://docs.bundlr.network/docs/client/cli#fund-a-bundlr-node for details.')

    throw new Error('')
  }

  const tx = bundlr.createTransaction(data, { tags: [{ name: 'Content-Type', value: contentType }]})
  await tx.sign()
  const response = await tx.upload()

  let state: State

  if (response.status === 200 || response.status === 202) {
    console.log(fgYellow, logPrefix(reveal), `Successfully uploaded ${filename} to Arweave.`)
    state = successState
  } else {
    console.log(fgMagenta, logPrefix(reveal), `Failed to upload ${filename}. Will retry.`)
    state = reveal.state
  }

  return {
    itemId: tx.id,
    state
  }
}

const uploadImage = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  const jwk = JSON.parse(fs.readFileSync(config.arweave.privateKeyFile).toString())
  const bundlr = new Bundlr('http://node1.bundlr.network', 'arweave', jwk)

  const {itemId, state} = await uploadFile(
    bundlr,
    reveal,
    State.ImageUploadSubmitted,
    `${config.local.imageDirectory}/${reveal.imageName}`,
    'image/png'
  )

  return {
    ...reveal,
    state,
    imageArweaveTx: itemId,
    verificationAttempts: 0
  }
}

const verifyImageUpload = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  console.log(fgCyan, logPrefix(reveal), 'Comparing remote & local image files.')
  console.log(fgYellow, logPrefix(reveal), 'This is delayed as Arweave is slow, please be patient.')

  await delay(5000)
  const response = await axios
    .get(`https://arweave.net/${reveal.imageArweaveTx}?ext=png`)
    .catch(_err => { return { status: 1000, data: 'Failed to fetch.' }})
  const img = fs.readFileSync(`${config.local.imageDirectory}/${reveal.imageName}`).toString()

  let state: State

  if (response.status == 200 && img == response.data) {
    console.log(fgYellow, logPrefix(reveal), 'Uploaded image is identical to the source file.')
    state = State.ImageUploadVerified
  } else {
    console.log(fgYellow, logPrefix(reveal), `Verification attempt ${reveal.verificationAttempts}/15 failed.`)
    state = reveal.verificationAttempts >= 15 ?
      State.NameAndTraitsExtracted :
      State.ImageUploadSubmitted
  }

  return {
    ...reveal,
    state,
    verificationAttempts: reveal.verificationAttempts + 1
  }
}

const generateOffChainDataFile = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  const data = {
    name: reveal.name,
    symbol: config.nft.symbol,
    description: config.nft.description,
    seller_fee_basis_points: config.nft.sellerFeeBasisPoints,
    image: `https://arweave.net/${reveal.imageArweaveTx}?ext=png`,
    attributes: reveal.traits.map(t => { return { trait_type: t.traitName, value: t.traitValue }}),
    properties: {
      files: [
        { uri: `https://arweave.net/${reveal.imageArweaveTx}?ext=png`, type: 'image/png' }
      ],
      category: 'image',
      creators: config.nft.creators.map(c => { return { address: c.address, share: c.share }})
    }
  }

  const ocdJson = JSON.stringify(data, null, 2)
  fs.writeFile(
    `${config.local.offChainDataDirectory}/${reveal.mintAddress}.json`,
    ocdJson,
    (err) => { if (err) {console.log(fgMagenta, logPrefix(reveal), `Error saving off-chain data: ${ocdJson}`) }}
  )

  return {
    ...reveal,
    offChainDataJsonFile: `${config.local.offChainDataDirectory}/${reveal.mintAddress}.json`,
    state: State.OffChainDataFileGenerated
  }
}

const uploadOffChainDataFile = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  const jwk = JSON.parse(fs.readFileSync(config.arweave.privateKeyFile).toString())
  const bundlr = new Bundlr('http://node1.bundlr.network', 'arweave', jwk)

  const {itemId, state} = await uploadFile(
    bundlr,
    reveal,
    State.OffChainDataUploadSubmitted,
    `${config.local.offChainDataDirectory}/${reveal.mintAddress}.json`,
    'application/json'
  )

  return {
    ...reveal,
    state,
    offChainDataArweaveTx: itemId,
    verificationAttempts: 0
  }
}

const dataEquals = (reveal: Reveal, dataLocal: any, dataRemote: any): boolean => {
  if (JSON.stringify(dataLocal) != JSON.stringify(dataRemote)) {
    console.log(fgMagenta, logPrefix(reveal), "Local data:", JSON.stringify(dataLocal))
    console.log(fgMagenta, logPrefix(reveal), "Remote data:", JSON.stringify(dataRemote))
  }

  return JSON.stringify(dataLocal) == JSON.stringify(dataRemote)
}

const verifyOffChainDataFile = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  console.log(fgCyan, logPrefix(reveal), 'Comparing local & remote off-chain data.')
  console.log(fgYellow, logPrefix(reveal), 'This is delayed as Arweave is slow. Please be patient.')

  await delay(5000)
  const response = await axios
    .get(`https://arweave.net/${reveal.offChainDataArweaveTx}`)
    .catch(_err => { return { status: 1000, data: 'Failed to fetch.' }})
  const offChainData = JSON.parse(
    fs.readFileSync(`${config.local.offChainDataDirectory}/${reveal.mintAddress}.json`).toString()
  )

  let state: State

  if (response.status == 200 && dataEquals(reveal, offChainData, response.data)) {
    console.log(fgYellow, logPrefix(reveal), 'Uploaded data is identical to the source file.')
    state = State.OffChainDataUploadVerified
  } else if (response.data === 'OK') {
    console.log(fgMagenta, logPrefix(reveal), 'Encountered a common upload error. Retrying upload.')
    state = State.OffChainDataFileGenerated
  } else {
    console.log(fgYellow, logPrefix(reveal), `Verification attempt ${reveal.verificationAttempts}/15 failed.`)
    state = reveal.verificationAttempts >= 15 ?
      State.OffChainDataFileGenerated :
      State.OffChainDataUploadSubmitted
  }

  return {
    ...reveal,
    state,
    verificationAttempts: reveal.verificationAttempts + 1
  }
}

const generateMetabossDataFile = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  const data = {
    name: reveal.name,
    symbol: config.nft.symbol,
    uri: `https://arweave.net/${reveal.offChainDataArweaveTx}`,
    seller_fee_basis_points: config.nft.sellerFeeBasisPoints,
    creators: config.nft.creators.map(c => { return {
      address: c.address,
      verified: c.verified === 1,
      share: c.share
    }})
  }

  const mbdJson = JSON.stringify(data, null, 2)
  fs.writeFile(
    `${config.local.metabossDataDirectory}/${reveal.mintAddress}.json`,
    mbdJson,
    (err) => { if (err) { console.log(fgMagenta, logPrefix(reveal), `Error saving metaboss data: ${mbdJson}`) }}
  )

  return {
    ...reveal,
    dataJson: `${config.local.metabossDataDirectory}/${reveal.mintAddress}.json`,
    state: State.MetabossDataFileGenerated
  }
}

const uploadMetabossDataFile = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  console.log(fgCyan, logPrefix(reveal), 'Updating metaboss data.')
  const command = sprintf(
    '%s -r %s update data --keypair %s --account %s --new-data-file %s',
    config.local.pathToMetaboss,
    config.solana.rpcServer,
    config.solana.privateKeyFile,
    reveal.mintAddress,
    reveal.dataJson
  )

  const final = () => {
    console.log(fgCyan, logPrefix(reveal), 'You should now validate the data and run the metaboss command.')
    console.log(fgYellow, logPrefix(reveal), `Arweave image upload: https://arweave.net/${reveal.imageArweaveTx}?ext=png`)
    console.log(fgYellow, logPrefix(reveal), `Arweave metadata upload: https://arweave.net/${reveal.offChainDataArweaveTx}`)
    console.log(fgYellow, logPrefix(reveal), `Generated Metaboss data: ${reveal.dataJson}`)
    console.log(commandColors, logPrefix(reveal), 'Metaboss command: ', command)
  }

  return {
    ...reveal,
    state: State.MetabossSubmitted,
    verificationAttempts: 0,
    final 
  }
}

const verifyMetabossDataChange = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  return {
    ...reveal,
    state: State.Revealed
  }
}

const revealNextStep = async (reveal: Reveal, config: Config): Promise<Reveal> => {
  switch (reveal.state) {
    case State.Allocated:
      return await extractNameAndTraits(reveal, config)
    case State.NameAndTraitsExtracted:
      return await uploadImage(reveal, config)
    case State.ImageUploadSubmitted:
      return await verifyImageUpload(reveal, config)
    case State.ImageUploadVerified:
      return await generateOffChainDataFile(reveal, config)
    case State.OffChainDataFileGenerated:
      return await uploadOffChainDataFile(reveal, config)
    case State.OffChainDataUploadSubmitted:
      return await verifyOffChainDataFile(reveal, config)
    case State.OffChainDataUploadVerified:
      return await generateMetabossDataFile(reveal, config)
    case State.MetabossDataFileGenerated:
      return await uploadMetabossDataFile(reveal, config)
    case State.MetabossSubmitted:
      return await verifyMetabossDataChange(reveal, config)
    case State.Revealed:
    default:
      return reveal
  }
}

const process = async (db: Db): Promise<void> => {
  saveDb(db)

  const leftToProcess = db.reveals.filter(r => r.state !== State.Revealed)

  if (leftToProcess.length === 0) {
    console.log(fgCyan, `${db.reveals.length} reveals are ready to be processed.`)
    db.reveals.forEach(reveal => reveal.final && reveal.final())

    return
  }

  const reveals = await Promise.all(db.reveals.map(rv => revealNextStep(rv, db.config)))

  process({ ...db, reveals })
}

const run = async () => {
  const config: Config = JSON.parse(fs.readFileSync('./config.json').toString())
  let db: Db;

  if (fs.existsSync(config.local.databaseFile)) {
    db = await loadDb(config)
  } else {
    db = await initDb(config)
  }

  db = await allocate(db)

  await process(db)
}

run()

export default run
