export enum State {
  Allocated,
  NameAndTraitsExtracted,
  ImageUploadSubmitted,
  ImageUploadVerified,
  OffChainDataFileGenerated, 
  OffChainDataUploadSubmitted,
  OffChainDataUploadVerified,
  MetabossDataFileGenerated,
  MetabossSubmitted,
  Revealed
}
